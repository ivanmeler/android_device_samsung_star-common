# Copyright (C) 2018 The LineageOS Project
# SPDX-License-Identifier: Apache-2.0
# Device specific property overrides

# Latch unsignaled fence buffers

PRODUCT_PROPERTY_OVERRIDES += \
    debug.sf.latch_unsignaled=1

PRODUCT_PROPERTY_OVERRIDES += \
    wifi.direct.interface=p2p-dev-wlan0
